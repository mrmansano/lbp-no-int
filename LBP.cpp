/*
 * LBP.cpp
 *
 *  Created on: 29/07/2010
 *      Author: marcelo
 */

#include "LBP.h"
#include <math.h>

void LBP8n(Mat mat_in, Mat& mat_out, int n) {
	if (mat_in.empty()) {
		cerr << "\tERRO: Imagem de entrada em LBP não definida.\n";
		exit - 1;
	}

	if (mat_out.rows <= 0 || mat_out.cols <= 0) {
		cerr << "\tERRO: Imagem de entrada em LBP não definida.\n";
		exit - 1;
	}

	for (int i = 1; i < mat_in.rows - 1; i++) {
		for (int j = 1; j < mat_in.cols - 1; j++) {
			int v = (int) mat_in.at<uchar> (i, j);

			int bit1, bit2, bit4, bit8, bit16, bit32, bit64, bit128;

			bit1 = 1 * bit(mat_in, i - sqrt(2), j - sqrt(2), v);
			bit2 = 2 * bit(mat_in, i - 2, j, v);
			bit4 = 4 * bit(mat_in, i - sqrt(2), j + sqrt(2), v);
			bit8 = 8 * bit(mat_in, i, j + 2, v);
			bit16 = 16 * bit(mat_in, i + sqrt(2), j + sqrt(2), v);
			bit32 = 32 * bit(mat_in, i + 2, j, v);
			bit64 = 64 * bit(mat_in, i + sqrt(2), j - sqrt(2), v);
			bit128 = 128 * bit(mat_in, i, j - 2, v);

			uchar lbp_val = (uchar) (bit1 + bit2 + bit4 + bit8 + bit16 + bit32
					+ bit64 + bit128);

			mat_out.at<uchar> (i, j) = (uchar) lbp_val;
		}
	}
}

void calcHistLBP(Mat mat_lbp, int ix, int iy, vector<double>& hist) {

	vector<float> features_final;
	vector<float> lbp_array(256);

	hist.resize(59);
	int count = 0;
	for (int k = 0; k < 256; k++) {
		if (checaBit(k) == 0) {
			lbp_array[k] = count;
			hist[count] = 0;
			count++;
		} else {
			lbp_array[k] = 58;
			hist[58] = 0;
		}
	}

	for (int l = 0; l < mat_lbp.rows; l++) {
		for (int m = 0; m < mat_lbp.cols; m++) {
			int val = (int) mat_lbp.at<uchar> (l, m);
			hist[lbp_array[val]]++;
		}
	}
}

int bit(Mat mat_in, double ptx, double pty, int threshold) {
	if (ptx < 0 || pty < 0 || pty > mat_in.cols || ptx > mat_in.rows)
		return 0;

	double ver_ptx = ptx - ((int) ptx);
	double ver_pty = pty - ((int) pty);
	int val = 0;

	if (ver_ptx == 0.0 || ver_pty == 0.0)
		val = (int) mat_in.at<uchar> (ptx, pty);

	else {
		cv::Mat p(cv::Size(1, 1), CV_8UC1);
		getRectSubPix(mat_in, cv::Size(1, 1), cv::Point2f(pty, ptx), p);
		val = (int) p.at<uchar> (0, 0);
	}

	if (val < threshold)
		return 0;
	else
		return 1;
}

int checaBit(int lbp_value) {
	int i = lbp_value;
	int bit8 = (i % 2);
	int bit7 = ((i / 2) % 2);
	int bit6 = ((i / 4) % 2);
	int bit5 = ((i / 8) % 2);
	int bit4 = ((i / 16) % 2);
	int bit3 = ((i / 32) % 2);
	int bit2 = ((i / 64) % 2);
	int bit1 = ((i / 128) % 2);
	int bitVector[9] = { bit1, bit8, bit7, bit6, bit5, bit4, bit3, bit2, bit1 };

	int current = bitVector[0];
	int count = 0;
	for (i = 0; i < 9; i++) {
		if (current != bitVector[i])
			count++;
		current = bitVector[i];
	}
	if (count > 2)
		return -1;
	else
		return 0;
}

