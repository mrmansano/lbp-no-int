/*
 * LBP_8n_NoInt.hpp
 *
 *  Created on: Jun 1, 2011
 *      Author: marcelo
 */

#ifndef LBP_8N_NOINT_HPP_
#define LBP_8N_NOINT_HPP_

#include <iostream>
#include <vector>

#include "LBP.h"

using namespace std;
using namespace cv;

void LBP8nNoInt(Mat mat_in, Mat& mat_out, int n);
int bitNoInt(Mat mat_in, double ptx, double pty, int threshold);

#endif /* LBP_8N_NOINT_HPP_ */
