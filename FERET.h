/*
 * FERET.h
 *
 *  Created on: Jun 2, 2011
 *      Author: marcelo
 */

#ifndef FERET_H_
#define FERET_H_

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/objdetect/objdetect.hpp>

#include <boost/algorithm/string.hpp>
#include <boost/filesystem/operations.hpp>
#include <boost/filesystem/path.hpp>

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>

#include "LBP8nNoInt.hpp"

using namespace std;
using namespace boost;
using namespace boost::filesystem;
using namespace cv;

class FERET {
private:
	string pathToFERET;

public:
	FERET(string path_to_feret);
	virtual ~FERET();

	bool createDatabaseFromOriginal(string path_to_destination,
			string pose, int face_w = 128, int face_h = 128);
	bool separate(string m_path, int num_m, int num_f);
	void extractLBPFeature(string m_path, int div_x = 0, int div_y = 0,
			int redim_x = 128, int redim_y = 128,
			string method = "libsvm", bool ov = false);
private:
	vector<string> listDir(string m_path);
	bool extractFace(Mat mat_in, Mat& mat_out, CascadeClassifier& c);
	int verifySex(string sbj);
	bool verifyFERETPath();
};

#endif /* FERET_H_ */
