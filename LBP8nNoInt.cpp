/*
 * LBP_8n_NoInt.cpp
 *
 *  Created on: Jun 1, 2011
 *      Author: marcelo
 */
#include "LBP8nNoInt.hpp"


void LBP8nNoInt(Mat mat_in, Mat& mat_out, int n) {
	if (mat_in.empty()) {
		cerr << "\tERRO: Imagem de entrada em LBP não definida.\n";
		exit - 1;
	}

	if (mat_out.rows <= 0 || mat_out.cols <= 0) {
		cerr << "\tERRO: Imagem de entrada em LBP não definida.\n";
		exit - 1;
	}

	equalizeHist(mat_in, mat_in);

	for (int i = 1; i < mat_in.rows - 1; i++) {
		for (int j = 1; j < mat_in.cols - 1; j++) {
			int v = (int) mat_in.at<uchar> (i, j);

			int bit1, bit2, bit4, bit8, bit16, bit32, bit64, bit128;

			bit1 = 1 * bitNoInt(mat_in, i - sqrt(2), j - sqrt(2), v);
			bit2 = 2 * bitNoInt(mat_in, i - 2, j, v);
			bit4 = 4 * bitNoInt(mat_in, i - sqrt(2), j + sqrt(2), v);
			bit8 = 8 * bitNoInt(mat_in, i, j + 2, v);
			bit16 = 16 * bitNoInt(mat_in, i + sqrt(2), j + sqrt(2), v);
			bit32 = 32 * bitNoInt(mat_in, i + 2, j, v);
			bit64 = 64 * bitNoInt(mat_in, i + sqrt(2), j - sqrt(2), v);
			bit128 = 128 * bitNoInt(mat_in, i, j - 2, v);

			uchar lbp_val = (uchar) (bit1 + bit2 + bit4 + bit8 + bit16 + bit32
					+ bit64 + bit128);

			mat_out.at<uchar> (i, j) = (uchar) lbp_val;
		}
	}
}

int bitNoInt(Mat mat_in, double ptx, double pty, int threshold) {
	if (ptx < 0 || pty < 0 || pty > mat_in.cols || ptx > mat_in.rows)
		return 0;

	double ver_ptx = ptx - ((int) ptx);
	double ver_pty = pty - ((int) pty);
	int val = 0;

	if (ver_ptx == 0.0 || ver_pty == 0.0)
		val = (int) mat_in.at<uchar> (ptx, pty);

	else {
		val = (int)( mat_in.at<uchar> (ceil(ptx), ceil(pty)) +
				mat_in.at<uchar> (ceil(ptx), floor(pty)) +
				mat_in.at<uchar> (floor(ptx), ceil(pty)) +
				mat_in.at<uchar> (floor(ptx), floor(pty)) ) / 4;
	}

	if (val < threshold)
		return 0;
	else
		return 1;
}
