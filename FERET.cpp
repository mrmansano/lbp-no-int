/*
 * FERET.cpp
 *
 *  Created on: Jun 2, 2011
 *      Author: marcelo
 */

#include "FERET.h"

FERET::FERET(string path_to_feret) {
	pathToFERET = path_to_feret;
}

FERET::~FERET() {
	// TODO Auto-generated destructor stub
}

bool FERET::createDatabaseFromOriginal(string path_to_destination, string pose,
		int face_w, int face_h) {
	if (!verifyFERETPath()) {
		cerr << "ERRO: Diretorio " << pathToFERET
				<< " não é um diretorio FERET válido." << endl;
		return false;
	}

	string feret_img_path(pathToFERET + "/data/images/");

	if (!exists(path(path_to_destination))) {
		cerr << "ERRO: Diretorio de destino " << path_to_destination
				<< " não existe." << endl;
		return false;
	}

	vector<string> sub = listDir(feret_img_path);

	CascadeClassifier c;
	if (!c.load(
			"/usr/local/share/opencv/haarcascades/haarcascade_frontalface_alt.xml")) {
		cerr << "ERRO:Não foi possível abrir arquivo de classificação." << endl;
		return false;
	}

	for (vector<string>::iterator it = sub.begin(); it != sub.end(); it++) {

		if (is_directory(path(feret_img_path + (*it)))) {
			vector<string> sub_files = listDir(feret_img_path + (*it));
			string gender;
			if (verifySex((*it)) == 0)
				gender = "M";
			else
				gender = "F";

			cout << "\tSujeito " << (*it) << " é " << gender << endl;

			for (vector<string>::iterator it_files = sub_files.begin(); it_files
					!= sub_files.end(); it_files++) {
				if ((*it_files).c_str()[0] == '.')
					continue;

				vector<string> f_split;
				split(f_split, (*it_files), is_any_of("."));

				if (f_split[1] == "ppm") {
					vector<string> desc_file;
					split(desc_file, f_split[0], is_any_of("_"));

					if (desc_file[2] == pose) {
						cout << "\t\tPegando imagem " << (*it_files) << endl;
						string img_path = feret_img_path + (*it) + "/"
								+ (*it_files);
						string dest_img_path = path_to_destination + "/"
								+ gender + "_" + (*it_files);

						Mat mat_in = imread(img_path, 0);
						Mat mat_out, mat_save;
						if (!extractFace(mat_in, mat_out, c))
							continue;

						resize(mat_out, mat_save, Size(face_w, face_h));

						if (!imwrite(dest_img_path, mat_save)) {
							cerr << "ERRO: Não foi possível salvar arquivo : "
									<< dest_img_path << endl;
							return false;
						} else {
							cout << "\t\tImagem salva em " << dest_img_path
									<< endl;
						}

					}
				}
			}

		}
	}

	return true;
}

bool FERET::separate(string m_path, int num_m, int num_f) {
	int sbjs[1208];
	string cp_dir = m_path + "/spd/";
	vector<string> files = listDir(m_path);
	int count_m = 0, count_f = 0;

	for (vector<string>::iterator it = files.begin(); it != files.end(); it++) {
		if ((*it).c_str()[0] == '.')
			continue;
		if (is_directory(path(m_path + "/" + (*it))))
			continue;

		vector<string> tokens;
		split(tokens, (*it), is_any_of("_"));

		if (sbjs[atoi(tokens[1].c_str())] > 2)
			continue;
		else
			sbjs[atoi(tokens[1].c_str())]++;

		if (tokens[0] == "M")
			if (count_m < num_m)
				count_m++;
			else
				continue;
		else if (count_f < num_f)
			count_f++;
		else
			continue;

		copy_file(path(m_path + "/" + (*it)), path(cp_dir + "/" + (*it)));
		cout << "\tArquivo " << m_path + "/" + (*it) << " copiado para "
				<< cp_dir + "/" + (*it) << endl;
	}
	cout << "Femeas: " << count_f << "\tMachos" << count_m << endl;
}

void FERET::extractLBPFeature(string m_path, int div_x, int div_y, int redim_x,
		int redim_y, string method, bool ov) {

	string path_train = m_path + "/training";
	string path_test = m_path + "/test2";

	if (!exists(path(path_train)) || !exists(path(path_test))) {
		cerr << "ERRO: Diretorio " << m_path
				<< " não apresenta formato de amostras de FERET." << endl;
		exit - 1;
	}

	stringstream str_file_train;
	str_file_train << m_path << "/train_" << redim_x << "x" << redim_y << "_"
			<< div_x << "x" << div_y << ((ov) ? "ov" : "") << ".libsvm";
	string path_train_file = str_file_train.str();

	stringstream str_file_test;
	str_file_test << m_path << "/test2_" << redim_x << "x" << redim_y << "_"
			<< div_x << "x" << div_y << ((ov) ? "ov" : "") << ".libsvm";
	string path_test_file = str_file_test.str();

	/*
	 * 	Cria arquivo de treino;
	 */
	/*cout << "Criando arquivo de treino: " << endl;
	ofstream out_train;
	out_train.open(path_train_file.c_str());

	vector<string> files_train = listDir(path_train);

	for (vector<string>::iterator it = files_train.begin(); it
			!= files_train.end(); it++) {
		if ((*it).c_str()[0] == '.')
			continue;
		if (is_directory(path(m_path + "/" + (*it))))
			continue;

		int gender = 1;
		if ((*it).c_str()[0] == 'F')
			gender = -1;

		Mat mat_in = imread(path_train + "/" + (*it), 0);
		Mat mat_res(Size(redim_x, redim_y), CV_8UC1);
		resize(mat_in, mat_res, Size(redim_x, redim_y));

		Mat mat_lbp(mat_res.size(), CV_8UC1);

		cout << "\tExtraindo de " << path_train + "/" + (*it) << endl;
		LBP8nNoInt(mat_res, mat_lbp, 2);
		//LBP8n(mat_res, mat_lbp, 2);

		int count_data = 1;
		string data_line;

		stringstream str_s;
		str_s << ((gender == 1) ? "+" : "") << gender;

		int div = 1;
		if (ov)
			div = 2;

		for (int i = 0; i < mat_lbp.rows; i += (div_y / div)) {
			for (int j = 0; j < mat_lbp.cols; j += (div_x / div)) {
				if (((i + div_y) > mat_lbp.rows)
						|| ((j + div_x) > mat_lbp.cols))
					continue;
				vector<double> data;
				Mat mat_rec = mat_lbp(Rect(j, i, div_x, div_y));
				calcHistLBP(mat_rec, 0, 0, data);

				for (int val = 0; val < data.size(); val++) {
					str_s << " " << count_data++ << ":" << data[val];
				}
			}
		}

		str_s << "\n";
		cout << "\t\tCaracterísticas de " << (*it) << " escrito." << endl;
		out_train.write(str_s.str().c_str(), str_s.str().size());
	}

	out_train.close();
	*/
	/*
	 * 	Cria arquivo de teste;
	 */
	cout << "Criando arquivo de teste: " << endl;
	ofstream out_test;
	out_test.open(path_test_file.c_str());

	vector<string> files_test = listDir(path_test);

	for (vector<string>::iterator it = files_test.begin(); it
			!= files_test.end(); it++) {
		if ((*it).c_str()[0] == '.')
			continue;
		if (is_directory(path(m_path + "/" + (*it))))
			continue;

		int gender = 1;
		if ((*it).c_str()[0] == 'F')
			gender = -1;

		Mat mat_in = imread(path_test + "/" + (*it), 0);
		Mat mat_res;
		resize(mat_in, mat_res, Size(redim_x, redim_y));

		Mat mat_lbp(mat_res.size(), CV_8UC1);

		cout << "\tExtraindo de " << path_test + "/" + (*it) << endl;
		LBP8nNoInt(mat_res, mat_lbp, 2);
		//LBP8n(mat_res, mat_lbp, 2);

		int count_data = 1;
		string data_line;

		stringstream str_s;
		str_s << ((gender == 1) ? "+" : "") << gender;

		int div = 1;
		if (ov)
			div = 2;

		for (int i = 0; i < mat_lbp.rows; i += (div_y / div)) {
			for (int j = 0; j < mat_lbp.cols; j += (div_x / div)) {
				if (((i + div_y) > mat_lbp.rows)
						|| ((j + div_x) > mat_lbp.cols))
					continue;
				vector<double> data;
				Mat mat_rec = mat_lbp(Rect(j, i, div_x, div_y));
				calcHistLBP(mat_rec, 0, 0, data);

				for (int val = 0; val < data.size(); val++) {
					str_s << " " << count_data++ << ":" << data[val];
				}
			}
		}

		str_s << "\n";
		cout << "\t\tCaracterísticas de " << (*it) << " escrito." << endl;
		out_test.write(str_s.str().c_str(), str_s.str().size());
	}

	out_test.close();
}

vector<string> FERET::listDir(string m_path) {
	vector<string> files;

	path in_path(m_path);

	if (is_directory(in_path)) {
		directory_iterator end_it;
		for (directory_iterator dir_it(in_path); dir_it != end_it; dir_it++) {

			files.push_back((string) dir_it->path().filename());
		}
	}

	return files;
}

int FERET::verifySex(string sbj) {
	string file_path = pathToFERET + "/data/ground_truths/name_value/" + sbj
			+ "/" + sbj + ".txt";

	ifstream info_file;
	info_file.open(file_path.c_str(), ifstream::in);

	while (info_file.good()) {
		char line[100];
		vector<string> tokens;

		info_file.getline(line, 100);
		split(tokens, line, is_any_of("="));

		if (tokens[0] == "gender") {
			int gender = (tokens[1] == "Male") ? 0 : 1;
			info_file.close();
			return gender;
		}
	}
	info_file.close();
	return -1;
}

bool FERET::extractFace(Mat mat_in, Mat& mat_out, CascadeClassifier& c) {
	Mat mat_gray;
	vector<Rect> faces;
	if (mat_in.channels() == 3)
		cvtColor(mat_in, mat_gray, CV_BGR2GRAY);
	else
		mat_in.copyTo(mat_gray);
	//equalizeHist(mat_gray, mat_gray);

	c.detectMultiScale(mat_gray, faces, 1.1, 2, 0
	//|CV_HAAR_FIND_BIGGEST_OBJECT
			//|CV_HAAR_DO_ROUGH_SEARCH
			| CV_HAAR_SCALE_IMAGE, Size(100, 100));

	if (faces.empty())
		return false;
	else
		mat_out = mat_gray(faces[0]);

	return true;
}

bool FERET::verifyFERETPath() {
	bool data_ok = false, bin_ok = false;
	vector<string> sub_dirs = listDir(pathToFERET);

	for (vector<string>::iterator it = sub_dirs.begin(); it != sub_dirs.end(); it++) {
		if ((*it) == "binaries")
			bin_ok = true;
		else if ((*it) == "data")
			data_ok = true;
	}

	return (data_ok && bin_ok);
}
