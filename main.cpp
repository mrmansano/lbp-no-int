/*
 * main.cpp
 *
 *  Created on: Jun 1, 2011
 *      Author: marcelo
 */

#include <iostream>
#include <fstream>
#include <string>
#include <vector>

#include <boost/algorithm/string.hpp>

#include "FERET.h"

using namespace std;

int main(int argc, char **argv) {
	FERET base_dvd1("/media/Dados/FERET/colorferet/colorferet/dvd1/");
	//FERET base_dvd2("/media/Dados/FERET/colorferet/colorferet/dvd2/");
	//base_dvd1.createDatabaseFromOriginal("/media/Dados/FERET/amostras/", "fa");
	//base_dvd1.createDatabaseFromOriginal("/media/Dados/FERET/amostras/", "fb");
	//base_dvd2.createDatabaseFromOriginal("/media/Dados/FERET/amostras/", "fa");
	//base_dvd2.createDatabaseFromOriginal("/media/Dados/FERET/amostras/", "fb");

	//base_dvd1.separate("/media/Dados/FERET/amostras/", 212, 199);

	int w = 36;
	int h = 36;

	int res_x = 6;
	int res_y = 6;

	base_dvd1.extractLBPFeature("/media/Dados/FERET/amostras/spd/", res_x, res_y, w, h, "libsvm", false);

	return 0;
}
