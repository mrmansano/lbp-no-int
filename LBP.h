/*
 * LBP.h
 *
 *  Created on: 29/07/2010
 *      Author: marcelo
 */

#ifndef LBP_H_
#define LBP_H_


#include <iostream>
#include <vector>

#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"

using namespace std;
using namespace cv;

void LBP8n(Mat mat_in, Mat& mat_out, int n);
void calcHistLBP(Mat mat_lbp, int ix, int iy, vector<double>& hist);
int bit(Mat mat_in, double ptx, double pty, int threshold);
int checaBit(int lbp_value);

#endif /* LBP_H_ */
