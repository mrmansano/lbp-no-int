Local Binary Patterns Algorithm Optimized
-----------------------------------------------

Implementation of the LBP algorithm eliminating a block of unnecessary multiplication
operations, that enhance the speed of the algorithm.

** It is necessary to have access to the FERET face database to use the test application **
